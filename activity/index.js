console.log("Hello World!");

// Activity Proper
// Operation on two numbers
let num1 = parseInt(prompt("Please insert a number:"));
console.log(num1);

let num2 = parseInt(prompt("Please insert another number:"));
console.log(num2);

let add = num1 + num2;

function operation(num1, num2){
	if(add < 10){
		console.log("Performed addition as the total of the numbers are < 10");
		let sum = num1 + num2;
		console.warn("The sum is: ", sum);
	}
	else if(add >= 10 && add <= 20){
		console.log("Performed subtraction as the total of the numbers are in range of 10-20");
		let diff = num1 - num2;
		alert(`The differnce is: ${diff}`);
	}
	else if(add >= 21 && add <= 30){
		console.log("Performed multiplication as the total of the numbers are in range of 21-30");
		let prod = num1 * num2;
		alert(`The product is: ${prod}`);
	}
	else{
		console.log("Performed division as the total of the numbers are > 31");
		let quo = num1 / num2;
		alert(`The quotient is: ${quo}`);
	}
};

operation(num1, num2);

// Name and Age prompt
let name = prompt("What is your name?");
let age = prompt("How old are you?")

if((name.length == 0) || (age.length == 0 || age === 0 || age === "")){
	alert("Are you a time traveler?");
}
else{
	alert(`Username: ${name} , Age: ${age}`); 
};
// ((name.length === 0) || (age.length === 0)) ? alert("Are you a time traveler?") : alert("User's name and Age");
// Age legality
function isLegalAge(age){
	(age >= 18) ? alert("You are of Legal Age") : alert("You are not allowed here");
};

isLegalAge(age);

// switch case
switch(age){
	case "18":
		alert("You are now allowed to party");
		break;
	case "21":
		alert("You are now part of the adult society");
		break;
	case "65":
		alert("We thank you for your contribution to society");
		break;
	default:
		alert("Are you sure you're not an alien?");
};

// try-catch-finally
	try{
		alert(isLegalage(age));
	}
	catch(err){
		console.log(typeof err);
		console.warn(err.message);
	}
	finally{
		console.log("Fix error");
	}