console.log("Hello World!");

// Assignment Operator (=)
let assignmentNumber = 8;

// Addition Assignment Operator(+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

assignmentNumber += 2;
console.log(assignmentNumber); //12

// subtraction / multiplication / division (-=, *=, /=)
assignmentNumber -= 2;
assignmentNumber *= 2;
assignmentNumber /= 2;

// Arithmetic Operators (+, -, *, /, %)
// PEMDAS Rule
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation " + pemdas);

// increment and decrement (++ , --)
let z = 1;
// Prefix incrementation
// the new value is returned immediately
++z;
console.log(z); //2
// Postfix incrementation
// returns the previous value of the variables and add 1 to its actual value
z++;
console.log(z); //3

console.log(z++); //3
console.log(z); //4

console.log(++z); //5

// Prefix and Postfix decrementation
console.log(--z); //4 - prefix
console.log(z--); //4 - postfix
console.log(z); //3

// Type coercion
// -is the automatio or implicit conversion of values from one data type to another
let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion); // string - concatenate
console.log(typeof coercion);
// Adding a string to a number it will result in a string


let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
// the result is a number

let numE = true + 1;
console.log(numE);
console.log(typeof numE);
// the result is a number
// the boolean "true" is associated with the value 1

let numF = false + 1;
console.log(numF);
// the boolean "false" is associated with the value 0

// Camparison operators
// (==) - equality operator
let juan = "juan";
console.log("Equality Operator");
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == "1"); //true
console.log(0 == false); //true
console.log("juan" == "JUAN"); //false - case sensitive
console.log("juan" == juan); //true

// (===) Strict Equality operator
console.log("Strict Equality Operator");
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === "1"); //false - data type
console.log(0 === false); //false - number and boolean
console.log("juan" === "JUAN"); //false - case sensitive
console.log("juan" === juan); //true

//(!=) inequality operator
console.log("Inequality Operator");
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != "1"); //false
console.log(0 != false); //false
console.log("juan" != "JUAN"); //true
console.log("juan" != juan); //false

//(!==) strict inequality operator
console.log("Strict Inequality Operator");
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== "1"); //true
console.log(0 !== false); //true
console.log("juan" !== "JUAN"); //true
console.log("juan" !== juan); //false

// Relational Comparison Operator
let x = 500;
let y = 700;
let w = 8000;
let numString = "5500";

// Greater than (>)
console.log("Greater Than");
console.log(x > y); //false

// Less than (<)
console.log("Less Than");
console.log(y < y); //false
console.log(numString < 6000); //true - forced/type coercion to change string to a number
console.log(numString < 1000); // false

// greater than or equal to
console.log("Greater than or Equal to");
console.log(w >= w); //true

// Less than or equal to
console.log("Less than or Equal to");
console.log(y <= y); //true

// Logical Operators (&&, ||, !)
let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

// Logical AND Operator (&&)
// returns true if ALL operands are true
console.log("Logical AND Operator");
let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); //true

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 95 && isLegalAge;
console.log(authorization3);

// Logical OR Operator (||) - (Double Pipe)
// return true if AT LEAST ONE of the operands are true
console.log("OR Operator");
let	userLevel1 = 100;
let	userLevel2 = 65;
let userAge = 15;

let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement1); //true

// Logical NOT operator (!)
console.log("NOT Operator");
// turns a boolean into the opposite value

let opposite = !isAdmin;
console.log(opposite); //true - isAdmin original value is false
console.log(!isRegistered);

let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);

// Conditional Statement
// if else if and if statement

// if statement
// executes a statement if a specified condition is true
if(true){
	console.log("We just ran an if condition!");
};

let numG = 5;
if(numG <10){
	console.log("Hello");
};

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length >= 10 && isRegistered && isAdmin){
	console.log("Welcome to Game Online!");
} else{
	console.log("You are not ready");
};
// .length is a property of strings which determine the number of characters in the string

// else statement
// execute a statement if all other conditions are false
if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge){
	console.log("Thank you for joining the Noobies Guild")
} else{
	console.log("You are too strong to be a Noob");
};

// else if statement
// executes a statement if previous conditions are false

if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Welcome Noob");
} else if(userLevel3 > 25){
	console.log("You are too strong");
} else if (userAge3 < requiredAge){
	console.log("You are too young to join the Guild");
} else if(userName3.length <10){
	console.log("Username is too short");
} else {
	console.log("You are not ready");
};



// if, else if and else statement with functions
let n = 3;
console.log(typeof n);

function addNum(num1, num2){
	// check if the numbers being passe as an arguements are number types
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number types");
		console.log(num1 + num2);
	}else{
		console.log("One or both of the arguments are not numbers")
	};
};

addNum(5,"2");

// create a log-in function
// check if the username and password are string type
function login(username, password){
	if(typeof username === "string" && typeof password === "string"){
		console.log("Both arguments are string");
		/*
			nested if else
				will run if the parent if statement is able to agre to accomplish its condition.

			Mini-Activity
			add another condition to our nested if statement
				-chech if the username and password is 8 characters long.
				-alert "Thank you For loging in"
			and else statement which will run if both statement were not meant.
				-show an alert mesage says credentials "too short"

			stretch goals
			add an else if statement that if the username is less than 8characters
				-show an alert "username too short"
			add an else if statement that if the password is less than 8 characters
				-show an alert "password too short"
		*/
		if(username.length === 8 && password.length === 8){
			console.log("Thank you for loging in");
		} else if(username.length < 8 && password.length >=8){
			console.log("Username too short");
		} else if(password.length < 8 && username.length >=8){
			console.log("Password too short");
		} else{
			console.log("Credentials too short");
		};
	} else{
		console.log("One or both of the arguments are not string");
	};
};
login("jane", "janey123");

// function with return keyword
let message = 'No message'
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 61){
		return "Not a typhoon yet";
	}
	else if(windSpeed <= 61){
		return "Tropical Depression detected";
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical Storm detected";
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return "Severe tropical storm detected"
	} 
	else{
		return "Typhoon detected";
	}
};

message =determineTyphoonIntensity(68);
console.log(message);

if(message == "Tropical Storm detected"){
	console.warn(message);
};

// console.warn is a good way to print warning in our console that could help us developers act on a certain output within our code

// truthy and falsy
// falsy (undefined, null, "", NaN, -0)
if(false){
	console.log("Truthy");
};

let firstName = "jane";
let middleName = "doe";
let lastName ="smith";

console.log(firstName + " " + middleName + " " + lastName);

// Template Literals
console.log(`${firstName} ${middleName} ${lastName}`);

// Ternary Operator (ES6)
/*
	Syntax:
		(expression/condition) ? ifTrue : ifFalse;
		(expression/condition) ? ifTrue : ifFalse;
*/
// Single Statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log(ternaryResult);

5000 > 1000 ? console.log("price is over 1000") : console.log("price is less than 1000");

// Else if with ternary operator
let a = 7;
a === 5
? console.log("A")
: (a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"));

// multiple statement execution
let name;
function isOfLegalAge() {
	name = "john";
	return "You are of the legal age";
};

function isUnderAge() {
	name = "jane"
	return "You are under the age limit";
};

let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log(`Resut of ternary operator in functions: ${legalAge}, ${name}`);

// Switch statement
/*
	- can be used as an alternative to an if-else if-else statement.

	syntax:
		switch(expression){
			case value1:
				statement;
				break;
			case value2:
				statement;
				break;
			.
			.
			.
			case valuen:
				statement;
				break;
			default:
				statement;
		};
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
	case "monday":
		console.log("The color of the day is red");
		break;
	case "tuesday":
		console.log("The color of the day is orange");
		break;
	case "wednesday":
		console.log("The color of the day is yellow");
		break;
	case "thursday":
		console.log("The color of the day is green");
		break;
	case "friday":
		console.log("The color of the day is blue");
		break;
	case "saturday":
		console.log("The color of the day is indigo");
		break;
	case "sunday":
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day")
};

// try-catch-finally statement


function showIntensityAlert(windSpeed){
	try{
		// Attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch(error){
		console.log(typeof error);

		console.log(error.message);
	}
	finally{
		// continue to execute regardless of success or failure of code execution in the try block
		alert("Intensity updates will show new alert");
	}
};

showIntensityAlert(56);
